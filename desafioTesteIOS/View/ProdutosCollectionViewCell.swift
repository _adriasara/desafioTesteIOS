//
//  ProdutosCollectionViewCell.swift
//  desafioTesteIOS
//
//  Created by Ádria Cardoso on 04/07/2018.
//  Copyright © 2018 Ádria Cardoso. All rights reserved.
//

import UIKit

class ProdutosCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var precoProduto: UILabel!
    @IBOutlet weak var nomeProduto: UILabel!
    @IBOutlet weak var imagemProduto: UIImageView!
}
