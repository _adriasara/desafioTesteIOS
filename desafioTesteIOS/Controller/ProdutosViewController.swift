//
//  ProdutosViewController.swift
//  desafioTesteIOS
//
//  Created by Ádria Cardoso on 04/07/2018.
//  Copyright © 2018 Ádria Cardoso. All rights reserved.
//

import UIKit

class ProdutosViewController: ViewController {

    @IBOutlet weak var imagemProduto: UIImageView!
    @IBOutlet weak var nomeProduto: UILabel!
    @IBOutlet weak var descricaoProduto: UITextView!
    
    var nome: String! = nil
    var url: String! = nil
    var descricao: String! = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nomeProduto.text = nome
        descricaoProduto.text = descricao
        
        let url = URL(string: self.url)
        imagemProduto.sd_setImage(with: url, completed: nil)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
