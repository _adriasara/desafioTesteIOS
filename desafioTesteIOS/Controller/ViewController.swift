//
//  ViewController.swift
//  desafioTesteIOS
//
//  Created by Ádria Cardoso on 04/07/2018.
//  Copyright © 2018 Ádria Cardoso. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift
import SDWebImage

class ViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {

    @IBOutlet weak var carrinhoBarItem: UIBarButtonItem!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var celulaClicada: [String:Any] = [String:Any]()
    
    var products: [[String: Any]] = [[String: Any]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Adicionando o background das Views.
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "bg.png")!)
        self.collectionView?.backgroundColor = UIColor.clear
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(false)
        
        // Fazendo o loadData inicializar primeiro que a CollectionView
        loadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadData(){
        // Fazendo requisição Web através da Libary Alamofire
        let url = URL(string: "http://ec2-54-171-222-219.eu-west-1.compute.amazonaws.com:8484/lista/")
        Alamofire.request(url!).responseJSON { [unowned self] (response) in
            print(response.value as Any)
            if let responseValue = response.result.value as! [String: Any]?{
                let data = responseValue["data"] as! [String:Any]
                let products = data["products"] as! [[String:Any]]
                self.products = products
                self.collectionView?.reloadData()
            }
        }
    }


    // A partir daqui é feita a exibição e criação da CollectionView
    // 1. Contendo a quantidade de células na Collection conforme a quantidade de produtos no JSON
    // 2. Pegando a a célula clicad para exibição dos detalhes
    // 3. Retornando os componentes do JSON para exibição na tela
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }

    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let viewController = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "detail") as! ProdutosViewController
        celulaClicada = products[indexPath.row]
        viewController.nome = celulaClicada["name"] as! String
        viewController.descricao = celulaClicada["description"] as! String
        viewController.url = celulaClicada["url"] as! String
        
        navigationController?.pushViewController(viewController, animated: true)

    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! ProdutosCollectionViewCell
        if self.products.count > 0 {
            let eachProduct = self.products[indexPath.row]
            cell.nomeProduto?.text = (eachProduct["name"] as? String) ?? ""
            cell.precoProduto?.text = String(eachProduct["priceMinimum"] as! Int)
            let url = URL(string: eachProduct["url"] as! String)
            cell.imagemProduto.sd_setImage(with: url, completed: nil)
            
        }
        return cell
    }
    
    // Adicionando o toast no botão ao Adicionar um Carrinho que exportei através das classes do grupo de Extensions
    @IBAction func botaoAdicionarAoCarrinho(_ sender: Any) {
        self.view.makeToast("Adicionado ao carrinho", duration: 3.0, position: .center)
        carrinhoBarItem.addBadge(number: 1)
    }
    
    // Adicionando o toast no botão do menu que exportei através das classes do grupo de Extensions
    @IBAction func menuBotao(_ sender: Any) {
        self.view.makeToast("Menu aberto", duration: 3.0, position: .center)
    }
}

